﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    public string horizontalAxis = "Horizontal";
    public string jumpButton = "Jump";
    public string lungeButton = "";

    public float initialJumpPower = .25f;
    public float maxJumpTime = .1f;

    public string moveFloat = "Speed";
    public string jumpTrigger = "Jump";
    public string fallBool = "Falling";
    public string lungeTrigger = "Lunge";

    private Mover mover;
    private Animator anim;

    private float jumpTime = 0f;
    private bool jumping = false;

    private bool facingRight = true;

    // Use this for initialization
    void Start()
    {
        mover = GetComponent<Mover>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (jumpButton != "")
        {
            if (Input.GetButtonDown(jumpButton) && mover.grounded)
            {
                jumping = true;
                mover.jumpPower = initialJumpPower;
                anim.SetTrigger(jumpTrigger);
            }
            else if (Input.GetButton(jumpButton) && jumping && jumpTime < maxJumpTime)
            {
                jumpTime += Time.deltaTime;
                mover.jumpPower += Mathf.Lerp(0, 1 - initialJumpPower, Time.deltaTime / maxJumpTime);
            }
            else
            {
                jumping = false;
                jumpTime = 0;
            }
        }

        mover.lateralMovement = Input.GetAxis(horizontalAxis);

        var vel = mover.velocity;

        anim.SetFloat(moveFloat, Mathf.Abs(mover.lateralMovement));
        if (fallBool != "") anim.SetBool(fallBool, vel.y < -.1f);

        if (vel.x < -.1f && facingRight)
        {
            facingRight = false;
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else if (vel.x > .1f && !facingRight)
        {
            facingRight = true;
            GetComponent<SpriteRenderer>().flipX = false;
        }

        if (lungeButton != "")
        {
            if (Input.GetButtonDown(lungeButton) && mover.grounded)
            {
                mover.lungeDir = facingRight ? 1 : -1;
                anim.SetTrigger(lungeTrigger);
            }
        }
    }
}
