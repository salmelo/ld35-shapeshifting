﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour
{

    public float speed = 10f;
    public float jumpForce = 7f;
    public float lungeForce = 7;
    public float lungeAngle = 45;

    public LayerMask groundLayer = -1;
    public bool moveWhileGrounded = false;

    public float lateralMovement { get; set; }
    //public bool jumping { get; set; }
    public float jumpPower { get; set; }

    public bool grounded { get; private set; }

    public Vector2 velocity { get { return rb.velocity; } }

    public float lungeDir { get; set; }

    private Rigidbody2D rb;


    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        grounded = rb.IsTouchingLayers(groundLayer);

        if (!moveWhileGrounded || grounded) //moveWhileGrounded implies grounded
        {
            var vel = rb.velocity;

            vel.x = lateralMovement * speed;

            rb.velocity = vel;
        }

        var impulse = new Vector2();
        if (jumpPower > 0)
        {
            impulse.y = jumpForce * jumpPower;
            jumpPower = 0;
        }
        else if (lungeDir != 0)
        {
            impulse = Quaternion.AngleAxis(lungeAngle, Vector3.forward) * (transform.right * lungeForce);
            impulse.x *= lungeDir;

            lungeDir = 0;
        }


        rb.AddForce(impulse, ForceMode2D.Impulse);
    }
}
